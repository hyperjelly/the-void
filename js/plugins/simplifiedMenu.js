var SimplifiedMenu = SimplifiedMenu || {};
var Imported = Imported || {};

//=============================================================================
/*:
 * @plugindesc A general plugin for all customizations needed for this game
 * @author Gimmer
 *
 * @param ---Parameters---
 * @default
 *
 * @param Message Y Offset
 * @parent ---Parameters---
 * @type number
 * @min 0
 * @max 1000
 * @desc Adjusts the position of the message box on the Y axis
 * Default: 600
 * @default 600
 *
 * @param Choice Y Padding
 * @parent ---Parameters---
 * @type number
 * @min 0
 * @max 50
 * @desc Adjust padding space between choice box and dialog box
 * Default: 0
 * @default 0
 *
 * @param Player Action Icon X Offset
 * @parent ---Parameters---
 * @type number
 * @min 0
 * @max 1000
 * @desc Offset on the X axis for an actionIcon over the player
 * Default: 0
 * @default 0
 *
 * @param Player Action Icon Y Offset
 * @parent ---Parameters---
 * @type number
 * @min 0
 * @max 1000
 * @desc Offset on the Y axis for an actionIcon over the player
 * Default: 0
 * @default 0
 *
 * @param Item Window Height
 * @parent ---Parameters---
 * @type number
 * @min 0
 * @max 1000
 * @desc Height in pixels for the Item Window
 * Default:
 * @default 250
 *
 * @param Item Window Width
 * @parent ---Parameters---
 * @type number
 * @min 0
 * @max 1000
 * @desc Width in pixels for the Key Item Window
 * Default:
 * @default 250
 *
 * @param Key Item Window Height
 * @parent ---Parameters---
 * @type number
 * @min 0
 * @max 1000
 * @desc Height in pixels for the Item Window
 * Default:
 * @default 300
 *
 * @param Key Item Window Width
 * @parent ---Parameters---
 * @type number
 * @min 0
 * @max 1000
 * @desc Width in pixels for the Key Item Window
 * Default:
 * @default 300
 *
 * @param Title Music Fade Duration
 * @parent ---Parameters---
 * @type number
 * @min 0
 * @max 30
 * @desc the duration to fade in music. Not sure the unit, probably seconds.
 * Default: 5
 * @default 5
 *
 *
 * @param Blur Amount
 * @parent ---Parameters---
 * @type number
 * @min 1
 * @max 5
 * @desc How Much to blur when the system menu loads menu.
 * Default: 2
 * @default 2
 *
 * @param Text Delay
 * @parent ---Parameters---
 * @type number
 * @min 0
 * @max 10
 * @desc The number of frames to wait between each character print to slow it down. Make this 0 to not wait at all
 * Default: 2
 * @default 2
 *
 * @help
 * A general plugin for customizations of the game core for this game
 */
//=============================================================================

//Params
var parameters = PluginManager.parameters('simplifiedMenu');
SimplifiedMenu.messageY = parameters['Message Y Offset'];
SimplifiedMenu.playerOffsetX = parameters['Player Action Icon X Offset'];
SimplifiedMenu.playerOffsetY = parameters['Player Action Icon Y Offset'];
SimplifiedMenu.itemHeight = parameters['Item Window Height'];
SimplifiedMenu.itemWidth = parameters['Item Window Width']
SimplifiedMenu.keyItemHeight = parameters['Key Item Window Height'];
SimplifiedMenu.keyItemWidth = parameters['Key Item Window Width']
SimplifiedMenu.choicePadding = parseInt(parameters['Choice Y Padding']);
SimplifiedMenu.titleMusicFadeDuration = parseInt(parameters['Title Music Fade Duration']);
SimplifiedMenu.blurAmount = parseInt(parameters['Blur Amount'])
SimplifiedMenu.textDelay = parseInt(parameters['Text Delay'])


//Add a wait to all text
SimplifiedMenu._Window_Message_prototype_processCharacter = Window_Message.prototype.processCharacter;
Window_Message.prototype.processCharacter = function(textState) {
    SimplifiedMenu._Window_Message_prototype_processCharacter.call(this,textState);
    if(this._waitCount === 0 && !this._showFast){
        this._waitCount = SimplifiedMenu.textDelay;
    }
};

SimplifiedMenu._Window_Base_obtainEscapeCode = Window_Base.prototype.obtainEscapeCode;
Window_Base.prototype.obtainEscapeCode = function(textState) {
    textState.index++;
    var send = (Imported.YEP_MessageCore) ? !this._checkWordWrapMode : true;
    if(textState.text.slice(textState.index, textState.index+9).match(/textdelay/i)) {
        textState.index += 9;
        return (send ? "textdelay" : "");
    } else if(textState.text.slice(textState.index, textState.index+14).match(/resettextdelay/i)) {
        textState.index += 14;
        return (send ? "resettextdelay" : "");
    } else {
        textState.index--;
        return SimplifiedMenu._Window_Base_obtainEscapeCode.call(this, textState);
    }
};

SimplifiedMenu._Window_Message_processEscapeCharacter = Window_Message.prototype.processEscapeCharacter;
Window_Message.prototype.processEscapeCharacter = function(code, textState) {
    switch (code) {
        case 'textdelay':
            SimplifiedMenu.textDelay = this.obtainEscapeParam(textState);
            break;
        case 'resettextdelay':
            SimplifiedMenu.textDelay = parseInt(parameters['Text Delay']);
            break;
        default:
            SimplifiedMenu._Window_Message_processEscapeCharacter.call(this, code, textState);
            break;
    }
}

//Prevent menu from being called
Scene_Map.prototype.isMenuCalled = function(){
    return false;
}

//Overload existing menu
Window_MenuCommand.prototype.makeCommandList = function() {
    this.addMainCommands();
};

Window_MenuCommand.prototype.addMainCommands = function() {
    var enabled = this.areMainCommandsEnabled();
    if (this.needsCommand('item')) {
        this.addCommand(TextManager.item, 'item', enabled);
    }
    if (this.needsCommand('keyItem')) {
        this.addCommand(TextManager.keyItem, 'keyItem', enabled);
    }
};

// Removing the blur when opening the item menu, keep for system menu
SceneManager.snapForBackground = function() {
    this._backgroundBitmap = this.snap();
};

SimplifiedMenu._Scene_MenuBase_prototype_createBackground = Scene_MenuBase.prototype.createBackground;
Scene_MenuBase.prototype.createBackground = function() {
    SimplifiedMenu._Scene_MenuBase_prototype_createBackground.call(this);
    this._filter = new PIXI.filters.BlurFilter();
    this._filter.blur = 0;
    if(this.constructor.name === 'Scene_SystemMenu'){
        this._filter.blur = SimplifiedMenu.blurAmount;
    }
    this._backgroundSprite.filters = [this._filter];
};

//What happens when you click item
Scene_Menu.prototype.commandItem = function() {
    SceneManager.push(Scene_Item);
};

Scene_Item.prototype.create = function() {
    Scene_ItemBase.prototype.create.call(this);
    this.createHelpWindow();
    this.createItemWindow();
    this.createActorWindow();
    this._itemWindow.activate();
    this._itemWindow.selectLast();
};

Scene_Item.prototype.createItemWindow = function(category) {
    var wy = this._helpWindow.height;
    this._itemWindow = new Window_ItemList(0, wy, Number(SimplifiedMenu.itemWidth), Number(SimplifiedMenu.itemHeight));
    this._itemWindow.setHelpWindow(this._helpWindow);
    this._itemWindow.setCategory('item');
    this._itemWindow.setHandler('ok',     this.onItemOk.bind(this));
    this._itemWindow.setHandler('cancel', this.popScene.bind(this));
    this.addWindow(this._itemWindow);
};

Scene_Item.prototype.createHelpWindow = function() {
    this._helpWindow = new Window_Help(2,Number(SimplifiedMenu.itemWidth));
    this.addWindow(this._helpWindow);
};

Window_Help.prototype.initialize = function(numLines, width) {
    width = width || Graphics.boxWidth;
    var height = this.fittingHeight(numLines || 2);
    Window_Base.prototype.initialize.call(this, 0, 0, width, height);
    this._text = '';
};



Scene_Menu.prototype.create = function() {
    Scene_MenuBase.prototype.create.call(this);
};

//Support for Key Items having their own menu choice
Scene_Menu.prototype.commandKeyItem = function() {
    SceneManager.push(Scene_Key_Item);
};

function Scene_Key_Item() {
    this.initialize.apply(this, arguments);
}


//Extended scenes
Scene_Key_Item.prototype = Object.create(Scene_Item.prototype);
Scene_Key_Item.prototype.constructor = Scene_Key_Item;

Scene_Key_Item.prototype.initialize = function() {
    Scene_Item.prototype.initialize.call(this);
};

Scene_Key_Item.prototype.create = function() {
    Scene_Item.prototype.create.call(this);
    this._itemWindow.setCategory('keyItem');
    this._itemWindow.selectLast();
};

Scene_Key_Item.prototype.createItemWindow = function() {
    var wy = this._helpWindow.height;
    this._itemWindow = new Window_ItemList(0, wy, Number(SimplifiedMenu.keyItemWidth), Number(SimplifiedMenu.keyItemHeight));
    this._itemWindow.setHelpWindow(this._helpWindow);
    this._itemWindow.setHandler('ok',     this.onItemOk.bind(this));
    this._itemWindow.setHandler('cancel', this.popScene.bind(this));
    this.addWindow(this._itemWindow);
};

Scene_Key_Item.prototype.createHelpWindow = function() {
    this._helpWindow = new Window_Help(2,Number(SimplifiedMenu.keyItemWidth));
    this.addWindow(this._helpWindow);
};

Scene_Key_Item.prototype.determineItem = function(){
    var item = this.item();
    if($gameParty.numItems(item) > 1){
        SoundManager.playEquip();
        if(item.meta.hasOwnProperty('Book')){
            Scene_Book.start(item.meta.Book);
        }
        else if(item.meta.hasOwnProperty('Picture')){
            this.popScene();
            this.popScene();
            //Variable 6 is the picture data
            $gameVariables.setValue(6, item.meta.Picture);
            //common event 2 is Show Note
            $gameTemp.reserveCommonEvent(2);
            this.checkCommonEvent();
        }
        else{
            throw 'Chosen item has no associated Book or Event metadata';
        }
    }
    else{
        this.activateItemWindow();
    }
}

//Requires yanfly core to work
if(Imported && Imported.YEP_MessageCore) {
    var Window_Help_prototype_setText = Window_Help.prototype.setText;
    Window_Help.prototype.setText = function (text) {
        if ($gameSystem.wordWrap()) text = '<WordWrap>' + text;
        Window_Help_prototype_setText.call(this, text);
    };
}

var Scene_Item_prototype_onItemOk = Scene_Item.prototype.onItemOk;
Scene_Key_Item.prototype.onItemOk = function(){
    Scene_Item_prototype_onItemOk.call(this);
}

//Eliminate counts from all item lists
Window_ItemList.prototype.needsNumber = function() {
    return false;
};

Window_ItemList.prototype.playOkSound = function() {
    //The sound ... of silence
};

//Don't draw name of items if you only have 1 and it's a key item
Window_Base.prototype.drawItemName = function(item, x, y, width) {
    width = width || 312;
    if (item) {
        var iconBoxWidth = Window_Base._iconWidth + 4;
        this.resetTextColor();
        this.drawIcon(item.iconIndex, x + 2, y + 2);
        if(item.itypeId === 2 && $gameParty.numItems(item) <= 1){
            this.drawText("??????", x + iconBoxWidth, y, width - iconBoxWidth);
        }
        else{
            this.drawText(item.name, x + iconBoxWidth, y, width - iconBoxWidth);
        }

    }
};

//Don't draw description of unknown items
Window_Help.prototype.setItem = function(item) {
    if(item && item.itypeId === 2 && $gameParty.numItems(item) <= 1){
        this.setText('??????');
    }
    else{
        this.setText(item ? item.description : '');
    }

};

//Enabled is different for key items
Window_ItemList.prototype.isEnabled = function(item) {
    if(item && item.itypeId === 2 && $gameParty.numItems(item) <= 1){
        return false;
    }
    return $gameParty.canUse(item);
};


//Simplify audio options
Window_Options.prototype.addVolumeOptions = function() {
    this.addCommand(TextManager.bgmVolume, 'bgmVolume');
    this.addCommand(TextManager.seVolume, 'seVolume');
};


//Add in a to desktop quit
Window_GameEnd.prototype.makeCommandList = function() {
    this.addCommand(TextManager.toTitle, 'toTitle');
    this.addCommand('To Desktop', 'toDesktop');
    this.addCommand(TextManager.cancel,  'cancel');
};


Scene_GameEnd.prototype.createCommandWindow = function() {
    this._commandWindow = new Window_GameEnd();
    this._commandWindow.setHandler('toTitle',  this.commandToTitle.bind(this));
    this._commandWindow.setHandler('toDesktop',  this.commandToDesktop.bind(this));
    this._commandWindow.setHandler('cancel',   this.popScene.bind(this));
    this.addWindow(this._commandWindow);
};

//Remove always dash and remember last command options
//Other plugins extend the default, so it has to be left here but blank
Window_Options.prototype.addGeneralOptions = function() {

};

//YEP_ButtonCommonEvents hardcodes keymapper for up to PAGEDOWN. Do that, but then swap it to up like we're doing to support WASD controls
var Input_revertButton = Input._revertButton;
Input._revertButton = function(button){
    Input_revertButton.call(this,button);
    if(button === 'PAGEDOWN'){
        this.keyMapper[87] = 'up';
    }
    else if(button === 'ALL'){
        this.keyMapper[87] = 'up';
    }
};

// Mapping Menu & Movement
(function () {
    // Menu
    Input.keyMapper[27] = 'system';     // esc - open menu
    // Movement
    Input.keyMapper[65] = 'left';     // a - move left
    Input.keyMapper[68] = 'right';    // d - move right
    Input.keyMapper[87] = 'up';       // w - move up
    Input.keyMapper[83] = 'down';     // s - move down

    //Always center (commented for now)
    // Window_Message.prototype.processAlign = function(textState) {
    //     textState = textState || this._textState;
    //     this.setAlignCenter(textState);
    // };

})();

// Limit Save Slots to 3
DataManager.maxSavefiles = function () {
  return 3;
};

//Gain every key item from the start
var DataManager_setupNewGame_prototype = DataManager.setupNewGame;
DataManager.setupNewGame = function() {
    DataManager_setupNewGame_prototype.call(this);
    $dataItems.forEach(item => {
        //console.log(item);
        if(item && item.itypeId === 2){
            $gameParty.gainItem(item,1);
        }
    });
};

// Stop the blinking cursor
Window.prototype._updateCursor = function() {
    this._windowCursorSprite.visible = this.isOpen();
};

//Extend Yanfl_X_extMEsPack1
var YEP_Window_Message_prototype_convertMessagePositions = Window_Message.prototype.convertMessagePositions;
Window_Message.prototype.convertMessagePositions = function(text){
    $gameSystem.setMessagePositionY(SimplifiedMenu.messageY);
    return YEP_Window_Message_prototype_convertMessagePositions.call(this,text);
}

//Make choice windows above text, with optional padding
var Window_ChoiceList_prototype_updatePlacement = Window_ChoiceList.prototype.updatePlacement;
Window_ChoiceList.prototype.updatePlacement = function(){
    Window_ChoiceList_prototype_updatePlacement.call(this);
    if($gameMessage.positionType() === 2){
        this.y = this._messageWindow.y - this.height;
        if(SimplifiedMenu.choicePadding > 0){
            this.y -= SimplifiedMenu.choicePadding;
        }
    }
}

//Extend Action Icons to support dynamic values
Galv.AI.checkEventForIcon = function(event) {
    var icon = 0;

    if (event.page()) {
        var listCount = event.page().list.length;

        for (var i = 0; i < listCount; i++) {
            if (event.page().list[i].code === 108) {
                var iconCheck = event.page().list[i].parameters[0].match(/<actionIcon: (.*)>/i);
                if (iconCheck) {
                    let returnObj = {'eventId': event._eventId, 'iconId' : -1};
                    iconCheck = iconCheck[1].split("|");
                    iconCheck.forEach(function(val){
                        val = val.trim();
                        if(val === 'PC'){
                            returnObj['offsetX'] = Number(SimplifiedMenu.playerOffsetX);
                            returnObj['offsetY'] = Number(SimplifiedMenu.playerOffsetY);
                            returnObj['isPlayer'] = true;
                        }
                        else if(val.contains('iconUp:') && $gamePlayer.direction() === 8){
                            val = val.split(":");
                            returnObj['iconId'] = Number(val[1]);
                        }
                        else if(val.contains('iconDown:') && $gamePlayer.direction() === 2){
                            val = val.split(":");
                            returnObj['iconId'] = Number(val[1]);
                        }
                        else if(val.contains('iconLeft:') && $gamePlayer.direction() === 4){
                            val = val.split(":");
                            returnObj['iconId'] = Number(val[1]);
                        }
                        else if(val.contains('iconRight:') && $gamePlayer.direction() === 6){
                            val = val.split(":");
                            returnObj['iconId'] = Number(val[1]);
                        }
                        else if(val.contains('x')){
                            val = val.split(":");
                            returnObj['offsetX'] = Number(val[1]);
                        }
                        else if(val.contains('y')){
                            val = val.split(":");
                            returnObj['offsetX'] = Number(val[1]);
                        }
                        else if(parseInt(val) > 0 && returnObj['iconId'] < 0){
                            returnObj['iconId'] = Number(val);
                        }
                    });
                    if(returnObj['iconId'] < 0){
                        return null;
                    }
                    return returnObj
                };
            };
        };
    };
    return null;
};

Game_Party.prototype.hasItem = function(item, includeEquip) {
    if (includeEquip === undefined) {
        includeEquip = false;
    }
    if (this.numItems(item) > 1 || (item.itypeId === 1 && this.numItems(item) > 0)) {
        return true;
    } else if (includeEquip && this.isAnyMemberEquipped(item)) {
        return true;
    } else {
        return false;
    }
};

//Stop title music from playing on the actual title
// Scene_Title.prototype.playTitleMusic = function() {
// };

Bitmap.prototype._drawTextOutline = function() {

};

//Scene_Splash plays the title music
if(typeof Scene_Splash !== "undefined"){
    var Scene_Splash_prototype_start = Scene_Splash.prototype.start;
    Scene_Splash.prototype.start = function () {
        Scene_Splash_prototype_start.call(this);
        this.playTitleMusic();
    }

//Title Music fades in over provided duration
    Scene_Splash.prototype.playTitleMusic = function () {
        AudioManager.playBgm($dataSystem.titleBgm);
        AudioManager.stopBgs();
        AudioManager.stopMe();
        AudioManager.fadeInBgm(SimplifiedMenu.titleMusicFadeDuration);
    }
}

//Add new commands to the title
Window_TitleCommand.prototype.makeCommandList = function() {
    this.addCommand(TextManager.newGame,   'newGame');
    this.addCommand(TextManager.continue_, 'continue', this.isContinueEnabled());
    this.addCommand('Credits','credits');
    this.addCommand(TextManager.options,   'options');
    this.addCommand('Quit','desktop');
};

//
Scene_Title.prototype.createCommandWindow = function() {
    this._commandWindow = new Window_TitleCommand();
    this._commandWindow.setHandler('newGame',  this.commandNewGame.bind(this));
    this._commandWindow.setHandler('continue', this.commandContinue.bind(this));
    this._commandWindow.setHandler('options',  this.commandOptions.bind(this));
    this._commandWindow.setHandler('credits',  this.commandCredits.bind(this));
    this._commandWindow.setHandler('desktop',  this.commandToDesktop.bind(this));
    this.addWindow(this._commandWindow);
};


//Command Credits
Scene_Title.prototype.commandCredits = function() {
    $gameTemp.reserveCommonEvent(12);
    $gamePlayer._opacity = 0
    $gamePlayer.reserveTransfer(8,1,1);
    this.startFadeOut(this.slowFadeSpeed());
    SceneManager.push(Scene_Map);
};

//Global Command to Desktop
Scene_Base.prototype.commandToDesktop = function() {
    this.fadeOutAll();
    SceneManager.exit();
};