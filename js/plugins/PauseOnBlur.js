// PauseOnBlur.js
// Created on 9/27/2018

var objYeth = objYeth || {};

/*:
* @plugindesc This plugin is meant to pause the game
* when the game window loses focus.
* @author Yethwhinger
*
* @help This plugin saves the BGM and BGS that are playing
* when the game window loses focus and stops them. It also
* stops updating the game. It resumes updates and replays
* the BGM and BGS when the window regains focus.
*/

objYeth.pauseAudio = function () {
    objYeth._pausedBgm = AudioManager.saveBgm();
    objYeth._pausedBgs = AudioManager.saveBgs();
    AudioManager.stopAll();
};

objYeth.resumeAudio = function () {
    if (objYeth._pausedBgm) {
        AudioManager.replayBgm(objYeth._pausedBgm);
    }
    if (objYeth._pausedBgs) {
        AudioManager.replayBgs(objYeth._pausedBgs);
    }
};

objYeth.pauseGame = function () {
    objYeth.pauseAudio();
    objYeth._gamePaused = true;
};

objYeth.resumeGame = function () {
    objYeth.resumeAudio();
    objYeth._gamePaused = false;
    SceneManager.update();
};

objYeth._gamePaused = false;
window.onblur = objYeth.pauseGame;
window.onfocus = objYeth.resumeGame;

//----------------------------
// Changes to SceneManager
//----------------------------

objYeth.SceneManager_update = SceneManager.update;
SceneManager.update = function () {
    if (!objYeth._gamePaused) {
        objYeth.SceneManager_update.call(this);
    }
};